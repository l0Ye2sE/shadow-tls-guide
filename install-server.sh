#!/bin/bash

echo "Select an option"
select opt in install change_domain_and_port uninstall quit
do
  case $opt in
    install)
      echo "Enter domain and press enter"
      read -r -e SITE
      echo "Enter port number 1-65535 Or type random for random port then press enter"
      read -r -e PORT
      if (( "$PORT" < 1 || "$PORT" > 65535)); then
        if [ "$PORT" != "random" ]
        then
          echo "Invalid port number"
          exit
        else
          PORT=$((1024 + $RANDOM))
        fi
      fi

      if [ -n "$(command -v apt)" ]; then
        apt update -y &&
        apt install -y ca-certificates curl gnupg lsb-release &&
        mkdir -p /etc/apt/keyrings &&
        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
        echo \
          "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
          $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
        apt update -y &&
        apt install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin
      elif [ -n "$(command -v yum)" ]; then
        yum install -y yum-utils &&
        yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo &&
        yum install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
      else
        echo "apt or yum not found"
        exit
      fi
      sleep 1
      systemctl enable docker
      sleep 1
      systemctl restart docker 

      #generate passwords
      SHADOWSOCKS_PASSWORD=$(openssl rand -base64 24)
      SHADOWTLS_PASSWORD=$(openssl rand -base64 24)
      #install containers
      mkdir -p $HOME/shadow-tls &&
      cat << EOF > "${HOME}/shadow-tls/docker-compose.yml"
version: '2.4'
services:
  shadowsocks:
    image: shadowsocks/shadowsocks-libev
    container_name: shadowsocks-raw
    restart: always
    network_mode: "host"
    environment:
      - SERVER_PORT=24000
      - SERVER_ADDR=127.0.0.1
      - METHOD=chacha20-ietf-poly1305
      - "PASSWORD=${SHADOWSOCKS_PASSWORD}"
  shadow-tls:
    image: ghcr.io/ihciah/shadow-tls:latest
    restart: always
    network_mode: "host"
    environment:
      - MODE=server
      - LISTEN=0.0.0.0:${PORT}
      - SERVER=127.0.0.1:24000
      - TLS=${SITE}:443
      - "PASSWORD=${SHADOWTLS_PASSWORD}"
      - V3=1
EOF

      cd "${HOME}/shadow-tls"
      docker compose up -d
      echo "Installed!" 
      echo "site : ${SITE}"
      echo "port : ${PORT}"
      echo "shadowsocks password : ${SHADOWSOCKS_PASSWORD}"
      echo "shadow-tls password : ${SHADOWTLS_PASSWORD}"

      break;;
    change_domain_and_port)
      echo "Enter domain and press enter"
      read -r -e SITE
      echo "Enter port number 1-65535 Or type random for random port then press enter"
      read -r -e PORT
      if (( "$PORT" < 1 || "$PORT" > 65535)); then
        if [ "$PORT" != "random" ]
        then
          echo "Invalid port number"
          exit
        else
          PORT=$((1024 + $RANDOM))
        fi
      fi
      sed -i "s#TLS=.*:443#TLS=${SITE}:443#" "${HOME}/shadow-tls/docker-compose.yml" &&
      sed -i "s#LISTEN=.*#LISTEN=0.0.0.0:${PORT}#" "${HOME}/shadow-tls/docker-compose.yml" &&
      cd "${HOME}/shadow-tls"
      docker compose up -d
      echo "Updated!" 
      echo "site : ${SITE}"
      echo "port : ${PORT}"

      break;;
    uninstall)
      if [ -n "$(command -v apt)" ]; then
        apt purge -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin docker-ce-rootless-extras
      elif [ -n "$(command -v yum)" ]; then
        yum remove -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin docker-ce-rootless-extras
      fi
      rm -rf /var/lib/docker
      rm -rf /var/lib/containerd
      rm -rf "${HOME}/shadow-tls"
      echo "Uninstalled!" 
      break;;
    quit)
      break;;
    *)
      echo "Invalid option"
      break;;
  esac
done
