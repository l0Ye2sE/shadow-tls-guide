# Shadow-tls guide
> I am not an expert so this guide may have problems
## Table of Contents
- [Server](#server)
- [Client](#client)
  - [Linux-Mac-Windows](#linux-mac-windows)
  - [Linux with docker](#linux-with-docker)
  - [Android](#android)
  - [IOS](#ios)
## Server
> script is for centos,ubuntu and probably for debian
1. change user to root
```bash
sudo -s
```
2. install using script. this script prompts you for two things :
- domain
  - some domain that has TLS and is famous and is not banned for
  - this is the website that you are using it's certificate
- port
  - the port for listening on server.
```bash
bash <(curl -s https://codeberg.org/l0Ye2sE/shadow-tls-guide/raw/branch/main/install-server.sh)
```
## Client
### Linux-Mac-Windows
> if you are a Windows user check [this](https://www.ghacks.net/2017/08/14/turn-off-smart-multi-homed-name-resolution-in-windows/) for fixing dns leak
1. [download](https://github.com/zzzgydi/clash-verge/releases) the client for your OS and install it
> open clash-verge with sudo or in windows run as administrator
2. go to setting and select **dns** and **tun** in **Clash Field**
![image](./images/clash-verge/1.jpg)
![image](./images/clash-verge/2.jpg)
3. in settings change **Clash Core** to **Clash Meta**
![image](./images/clash-verge/3.jpg)
![image](./images/clash-verge/4.jpg)

4. in settings active **Tun Mode**

![image](./images/clash-verge/5.jpg)

5. create a file named config.yaml with below content using notepad
```yml
dns:
  enable: true
  listen: 0.0.0.0:1053
  ipv6: true
  enhanced-mode: redir-host
  fake-ip-range: 28.0.0.1/8
  default-nameserver:
    - https://1.1.1.1/dns-query
    - https://8.8.8.8/dns-query
  nameserver:
    - 'tls://1.1.1.1:853#SHADOW_TLS_PROXY'
    - 'tls://8.8.8.8:853#SHADOW_TLS_PROXY'
  fallback:
    - 'tls://1.1.1.1:853#SHADOW_TLS_PROXY'
    - 'tls://8.8.8.8:853#SHADOW_TLS_PROXY'
  proxy-server-nameserver:
    - tls://1.1.1.1:853
    - tls://8.8.8.8:853
tun:
  enable: true
  stack: gvisor
  dns-hijack:
    - any:53
  auto-route: true
  auto-detect-interface: true
  strict-route: true
proxies:
- name: SHADOW_TLS_PROXY
  type: ss
  server: your_vps_ip #UPDATE
  port: port #UPDATE
  cipher: chacha20-ietf-poly1305
  password: "SHADOWSOCKS_PASSWORD" #UPDATE
  plugin: shadow-tls
  plugin-opts:
    host: www.example.com #UPDATE
    password: "SHAODW_TLS_PASSWORD" #UPDATE
    version: 3
proxy-groups:
- name: relay
  type: relay
  proxies:
  - SHADOW_TLS_PROXY
rules:
  - MATCH,SHADOW_TLS_PROXY
```

6. update the parts shown above and save the file

7. in **Profiles** choose **New**

![image](./images/clash-verge/6.jpg)

8. in **Type** choose **Local** then choose the config file and **SAVE**

![image](./images/clash-verge/7.jpg)

9. right click on the imported config and choose **Select** 

![image](./images/clash-verge/8.jpg)

### Linux with docker
1. make folder named shadow-tls and go inside that
```bash
mkdir shadow-tls && cd shadow-tls
```
2. create file named docker-compose.yml,put this content inside it and then update the specified lines
```yml
version: '2.4'
services:
  shadow-tls:
    image: ghcr.io/ihciah/shadow-tls:latest
    restart: always
    network_mode: "host"
    environment:
      - MODE=client
      - LISTEN=0.0.0.0:24000
      - SERVER=your_vps_ip:8443 # put your vps ip and port
      - TLS=cloud.tencent.com # change to domain of the web site you set in the server config
      - "PASSWORD=SHAODW_TLS_PASSWORD" # use the password use set in the server
      - V3=1
```
3. run docker compose up -d
4. install shadowsocks-libev
```bash
apt install -y shadowsocks-libev
```
5. create a file named config.json,put beloew content inside it and update **SHADOWSOCKS_PASSWORD**
```json
{
 "server":"127.0.0.1",
 "mode":"tcp_only",
 "server_port":24000,
 "local_address":"127.0.0.1",
 "local_port":1080,
 "password":"SHADOWSOCKS_PASSWORD",
 "timeout":60,
 "method":"chacha20-ietf-poly1305"
}
```
6. save the file then run shadowsocks-libev
```bash
ss-local -c ./shadowsocks-config.json
```
7. now you have a socks proxy on address 127.0.0.1:1080
### Android
1. download clash meta from [f-droid](https://f-droid.org/en/packages/com.github.metacubex.clash.meta) **not from google play** because it's some other app with the simillar name
2. put the below content in a config.yaml file and update the specified lines
```yml
proxies:
- name: SHADOW_TLS_PROXY
  type: ss
  server: your_vps_ip #UPDATE
  port: port #UPDATE
  cipher: chacha20-ietf-poly1305
  password: "SHADOWSOCKS_PASSWORD" #UPDATE
  plugin: shadow-tls
  plugin-opts:
    host: www.example.com #UPDATE
    password: "SHAODW_TLS_PASSWORD" #UPDATE
    version: 3
proxy-groups:
- name: relay
  type: relay
  proxies:
  - SHADOW_TLS_PROXY
rules:
  - MATCH,SHADOW_TLS_PROXY
```
3. in app go to **Profile**

![image](./images/clash-android/1.jpg)

4. tap **plus button** 

![image](./images/clash-android/2.jpg)

5. tap **File**

![image](./images/clash-android/3.jpg)

6. tap **Browse Files**

![image](./images/clash-android/4.jpg)

7. tap on the part shown on picture 
> You can see size of **Configuration.yaml** file is 0 Bytes

![image](./images/clash-android/5.jpg)

8. tap **Import** and choose config.yaml file

![image](./images/clash-android/6.jpg)

9. tap back button
> You can see size of **Configuration.yaml** file is not zero because it isn't empty

![image](./images/clash-android/7.jpg)

10. tap the part shown on picture to save profile

![image](./images/clash-android/8.jpg)

11. Select **New Profile**

![image](./images/clash-android/9.jpg)

12. tap back button

![image](./images/clash-android/10.jpg)

13. press **Tap to start**

![image](./images/clash-android/11.jpg)

### IOS
maybe shadowrocket supports it
